Rails.application.routes.draw do
    #namespace anschauen!
    resources :accounts, constraints: { id: /[a-zA-Z1-9\.@,\=]+/ } do
        get :delete, :on => :member
        get :password, :on => :member
        patch :password, :on => :member, :action => :passwordupdate
    end
    #get '/accounts/:id/password' => 'accounts#password', as: 'password_account'
    #patch '/accounts/:id/password' => 'accounts#passwordupdate'
    #get '/password/:id' => 'accounts#password', as: 'password'
    resources :groups do
        get :delete, :on => :member
    end

    resources :aliases, constraints: { id: /[a-zA-Z1-9\.@,\=]+/ } do
        get :delete, :on => :member
    end

    resource :profile do
        get :password
        patch :password, :action => :passwordupdate
        get 'group/:id' => 'profiles#group', as: 'group'
        get 'group/:id/leave' => 'profiles#groupleave', as: 'groupleave'
        delete 'group/:id/leave' => 'profiles#groupleft', as: 'groupleft'
    end

    #root 'sessions#new'
    #TODO: this doesn't seem to work:
    #get '/', :to => 'accounts#index', :constraints => :current_user_admin
    get '/', :to => 'profiles#index'
    get '/login' => 'sessions#new'
    post '/login' => 'sessions#create'
    get '/logout' => 'sessions#destroy'
    #get '/password/' => 'profiles#password', as: 'password'
    #patch '/password/' => 'profiles#passwordupdate'
    #get '/g/:id' => 'profiles#group', :as => 'profiles_group'
    #get '/g/:id/leave' => 'profiles#leavegroup', :as => 'profiles_group_leave'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
