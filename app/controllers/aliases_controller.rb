class AliasesController < ApplicationController
    before_filter :authorizeadmin

    def index
        @aliases = Alias.all(:limit => 50, :offset => params[:count].to_i, :sort_by => 'uid')
    end

    def new
        @alias = Alias.new
    end

    def create
        @alias = Alias.new(alias_params)
        @alias.forwardTo = Account.find(alias_params[:forwardTo]).dn
        begin
            if @alias.save
                redirect_to @alias
            else
                render 'new'
            end
        rescue ActiveLdap::LdapError::InsufficientAccess
            redirect_to aliases_path, alert: "Insufficient Access!"
        rescue ActiveLdap::LdapError::ConstraintViolation
            redirect_to aliases_path, alert: "Alias UIDs must be #{UIDCONST} and unique"
        end
    end

    def show
        begin
            @alias = Alias.find(params[:id], :attributes => ["+", "*"])
        rescue ActiveLdap::EntryNotFound
            redirect_to aliases_path, alert: "Alias #{params[:id]} not found." and return
        end
        render 'alias'
    end

    def delete
        @alias = Alias.find(params[:id])
    end

    def destroy
        Alias.destroy(params[:id])
        redirect_to aliases_path, notice: "Alias #{params[:id]} deleted."
    end


    private
    def alias_params
        params.require(:alias).permit(:uid, :forwardTo)
    end

end
