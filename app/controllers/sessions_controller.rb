class SessionsController < ApplicationController
    def new
    end

    def create
        require 'active_ldap'
        begin
            ActiveLdap::Base.setup_connection(:bind_dn => "uid="+params[:account]+",ou=Accounts,"+Rails.application.config.ldapbase, :base => Rails.application.config.ldapbase, :password => params[:password], :allow_anonymous => false)
            # wenn kein find gemacht wird, wird keine verbindung hergestellt
            # und es kommt zu keinem authentication error
            @user = Account.find(params[:account])
            session[:uid] = @user.uid
            redirect_to '/'
        rescue ActiveLdap::AuthenticationError
            redirect_to '/login', notice: 'Login Failed.' and return
        end
    end

    def destroy
        session[:uid] = nil
        redirect_to '/login'
    end
end
