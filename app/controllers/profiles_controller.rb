class ProfilesController < ApplicationController
    before_filter :authorize

#    def show
#        @account = Account.find(session[:uid], :attributes => ["+", "*"])
#        render 'index'
#    end

    def index
        @account = Account.find(session[:uid], :attributes => ["+", "*"])
        render 'account'
    end

    def group
        @group = Group.find(params[:id])
    end

    def groupleave
        @group = Group.find(params[:id])
    end

    def groupleft
        begin
            @group = Group.find(params[:id])
            ldif = ActiveLdap::LDIF.new
            record = ActiveLdap::LDIF::ModifyRecord.new(@group.dn)
            record.add_operation(:delete, "member", [], { "member" => Account.find(session[:uid]).dn.to_s })
            ldif << record
            ActiveLdap::Base.load(ldif.to_s)
            redirect_to profile_path, notice: "You left group #{@group.cn}." and return
        rescue ActiveLdap::OperationNotPermitted
            redirect_to profile_path, alert: "Operation not permitted." and return
        end
    end

    def password
        session[:return_to] = request.referer
        @account = Account.find(session[:uid])
    end

    def passwordupdate
        begin
            raise PasswordMismatch if password_params[:userpassword] != password_params[:userpassword_confirmation]
            raise PasswordEmpty if password_params[:userpassword].empty?
            conn = LDAP::Conn.new('localhost')
            conn.set_option(LDAP::LDAP_OPT_PROTOCOL_VERSION, 3)
            conn.bind("uid="+session[:uid]+",ou=Accounts,"+Rails.application.config.ldapbase, password_params[:oldpassword])
            @account = Account.find(session[:uid])
            salt = "$6$#{ActiveLdap::UserPassword::Salt.generate(22)}"
            @account.userPassword = ActiveLdap::UserPassword.crypt(params[:account][:password], salt)
            @account.save
            redirect_to session.delete(:return_to), notice: 'Password changed.' and return
        rescue PasswordEmpty
            redirect_to password_profile_path, alert: "Password mut not be empty!" and return
        rescue PasswordMismatch
            redirect_to password_profile_path, alert: "The Passwords don't match!" and return
        rescue LDAP::ResultError => e
            redirect_to password_profile_path, notice: "Old password was wrong. #{e.message}" and return
        rescue ActiveLdap::LdapError::InsufficientAccess
            redirect_to session.delete(:return_to), notice: "Password could not be changed." and return
        end
    end

    private
    def password_params
        params.require(:account).permit(:oldpassword, :userpassword, :userpassword_confirmation)
    end
end
