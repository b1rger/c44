class ApplicationController < ActionController::Base
    class PasswordMismatch < StandardError
    end
    class PasswordEmpty < StandardError
    end
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_user
      begin
          @current_user ||= Account.find(session[:uid]) if session[:uid]
      rescue ActiveLdap::ConnectionNotSetup 
          @current_user = false
      rescue ActiveLdap::EntryNotFound
          redirect_to '/logout'
      end
  end
  helper_method :current_user

  def authorize
      redirect_to '/login' unless current_user
  end

  def current_user_admin
      @current_user_admin = false
      if current_user
          begin
              current_user.groups.each do |group| 
                  @current_user_admin = true if group.dn == 'cn=hrpeople,ou=Groups,'+Rails.application.config.ldapbase
              end
          rescue ActiveLdap::ConnectionNotSetup
          end
      end
      return @current_user_admin
  end
  helper_method :current_user_admin

  def authorizeadmin
      not_found unless current_user_admin
  end

  def not_found
      redirect_to '/logout'
        #raise ActionController::RoutingError.new('Not Found')
  end
end
