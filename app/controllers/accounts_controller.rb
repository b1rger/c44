class AccountsController < ApplicationController
    before_filter :authorizeadmin

    def index
        @accounts = Account.all(:limit => 50, :offset => params[:count].to_i, :sort_by => 'uid', :attributes => ["+", "*"])
    end

    def new
        @account = Account.new
    end

    def create
        @account = Account.new(account_params)
        @account.userPassword = mkpassword(params[:account][:userpassword], params[:account][:userpassword_confirmation])
        begin
            if @account.save
                redirect_to @account
            else
                render 'new'
            end
        rescue ActiveLdap::LdapError::InsufficientAccess
            redirect_to accounts_path, alert: "Insufficient Access!"
        rescue ActiveLdap::LdapError::ConstraintViolation
            redirect_to accounts_path, alert: "Account UIDs must be #{UIDCONST} and unique"
        end
    end

    def show
        begin
            @account = Account.find(params[:id], :attributes => ["+", "*"])
        rescue ActiveLdap::EntryNotFound
            redirect_to accounts_path, alert: "Account #{params[:id]} not found." and return
        end
        render 'account'
    end

    def edit
        @account = Account.find(params[:id])
    end

    def update
        account = Account.find(account_params[:uid])
        account.openPGPpublicKey = account_params[:openPGPpublicKey]
        account.sshRSAAuthKey = account_params[:sshRSAAuthKey]
        account.sshED25519AuthKey = account_params[:sshED25519AuthKey]
        begin
            account.save
            redirect_to account_path(params[:account][:uid]), notice: "Account #{account.uid} updated."
        rescue ActiveLdap::LdapError::InsufficientAccess
            redirect_to account_path(params[:account][:uid]), alert: "Insufficient Access!"
        end
    end

    def destroy
        Account.destroy(params[:id])
        redirect_to accounts_path, notice: "Account #{params[:id]} deleted."
    end

    def password
        session[:return_to] ||= request.referer
        begin
            @account = Account.find(params[:id], :attributes => ["+", "*"])
        rescue ActiveLdap::EntryNotFound
            redirect_to accounts_path, notice: "Account not found."
        end
    end

    def passwordupdate
        begin
            raise PasswordMismatch if params[:account][:userpassword] != params[:account][:userpassword_confirmation]
            raise PasswordEmpty if params[:account][:userpassword].empty?
            @account = Account.find(params[:account][:uid])
            salt = "$6$#{ActiveLdap::UserPassword::Salt.generate(22)}"
            @account.userPassword = ActiveLdap::UserPassword.crypt(params[:account][:userpassword], salt)
            @account.save
            redirect_to session.delete(:return_to), notice: "Password changed." and return
        rescue AccountsController::PasswordEmpty
            redirect_to session.delete(:return_to), alert: "Password mut not be empty!" and return
        rescue AccountsController::PasswordMismatch
            redirect_to session.delete(:return_to), alert: "Password Mismatch!" and return
        rescue ActiveLdap::LdapError::InsufficientAccess
            redirect_to session.delete(:return_to), alert: 'Password could not be changed.' and return
        end
    end

    def delete
        @account = Account.find(params[:id])
    end

    protected
    def mkpassword(password, password_confirmation)
        raise PasswordMismatch if password != password_confirmation
        raise PasswordEmpty if password.empty?
        salt = "$6$#{ActiveLdap::UserPassword::Salt.generate(22)}"
        password = ActiveLdap::UserPassword.crypt(password, salt)
        password
    end

    private
    def account_params
        params.require(:account).permit(:uid, :userpassword, :openPGPpublicKey, :sshRSAAuthKey, :sshED25519AuthKey)
    end

    def initscrambler(password)
        iterations = 12
        blowfishscheme = '$2a$'
        salt = Base64.strict_encode64(random.bytes(16)).gsub('+', '.').slice 0, 22
        settings = "#{blowfishscheme}#{iterations}$#{salt}"
        hashed_password = BCrypt::Engine.hash_secret password, settings
        @key_pair = OpenSSL::PKey::RSA.new 2048
        @key_pair.to_pem(OpenSSL::Cipher.new('aes-256-cbc'), hashed_password).gsub /\n/, '_'
    end



end
