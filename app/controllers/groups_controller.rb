class GroupsController < ApplicationController
    before_filter :authorizeadmin

    def index
        @groups = Group.all(:limit => 50, :offset => params[:count].to_i, :sort_by => 'cn', :attributes => ["+", "*"])
    end

    def edit
        @group = Group.find(params[:id])
    end

    def new
        @group = Group.new
    end

    def create
        @group = Group.new(group_params[:cn])
        manage(@group)
    end

    def update
        @group = Group.find(group_params[:cn])
        manage(@group)
    end

    def show
        @group = Group.find(params[:id], :attributes => ["+", "*"])
        render 'group'
    end

    def destroy
        @group = Group.find(params[:id])
        @group.destroy
        redirect_to groups_path, notice: "Group deleted."
    end

    def delete
        @group = Group.find(params[:id])
    end

    private
    def group_params
        params.require(:group).permit(:cn, :description, :members => [])
    end
    def manage(group)
        group.description = group_params[:description]
        group.members.each { |m| group.members.delete(Account.find(m.uid)) }
        group_params[:members].each do |member| group.members.push(Account.find(member)) end
        begin
            if group.save
                redirect_to group
            else
                render 'edit'
            end
        rescue ActiveLdap::LdapError::InsufficientAccess
            redirect_to groups_path, alert: "Insufficient Access!" and return
        end
    end

end
