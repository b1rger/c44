class Group < ActiveLdap::Base
  ldap_mapping dn_attribute: "cn",
               prefix: "ou=Groups",
               classes: ["groupOfNames"]
  has_many :members, :class_name => "Account",
      :wrap => "member", :primary_key => "dn"
  validates :cn, presence: true, format: { with: /\A[[:alnum:]]+\z/i }
  #validates :description, presence: true
  validates :members, presence: true
end
