class Alias < ActiveLdap::Base
  ldap_mapping dn_attribute: "uid",
               prefix: "ou=Aliases",
               classes: ["account", "c44Alias"]
  validates :uid, presence: true, :format => {:with => /\A[[:alnum:].-]+@(ourdomain.tld|anotherdomain.tld)\z/i }
end
