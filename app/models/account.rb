class Account < ActiveLdap::Base
  ldap_mapping dn_attribute: "uid",
               prefix: "ou=Accounts",
               classes: ["c44Account", "simpleSecurityObject", "scrambler"],
               scope: :sub
  belongs_to :groups, :class_name => "Group",
      :many => "member", :foreign_key => "dn"
  validates :uid, presence: true, :format => {:with => /\A[[:alnum:].-]+@(ourdomain.tld|anotherdomain.tld)\z/i }
end
